import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './modules/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './modules/auth-layout/auth-layout.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './shared_components/components.module';
import { FleetLayoutComponent } from './modules/fleet-layout/fleet-layout.component';
import { RegisterHeaderComponent } from './modules/auth-layout/register/register-header/register-header.component';
import { Events } from '../shared/events/events';
import { RegisterService } from '../shared/services/register-service.service';
import { HttpErrorHandlerService } from '../shared/services/http-error-handler.service';
import { LoginService } from '../shared/services/login.service';
import {NgxPaginationModule} from 'ngx-pagination';
import { AgmCoreModule } from '@agm/core';



@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule, 
    NgxPaginationModule, 
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyCoSEwNEhAcZ2YtV9D25MLwqal1FA3Gsm4'
    })  
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    FleetLayoutComponent,
    RegisterHeaderComponent

  ],
  providers: [Events,RegisterService,HttpErrorHandlerService,LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
