import { Component, OnInit, OnDestroy } from '@angular/core';
import { Credential } from 'src/shared/models/Credential';
import { LoginService } from 'src/shared/services/login.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  credentials: Credential;
  failLogging: boolean;
  message: string;
  constructor(public loginService: LoginService, public router: Router) {
    this.setMessage();
    this.credentials = new Credential();
  }
  ngOnInit() {
  }
  ngOnDestroy() {
  }
  setMessage() {
    if (this.loginService.isLoggedIn) {
      this.message = 'Logged in';
    }
  }

  login() {
    this.message = 'Trying to log in ...';

    this.loginService.login(this.credentials).subscribe(() => {
      this.setMessage();
      if (this.loginService.isLoggedIn) {
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        let redirect = this.loginService.redirectUrl ? this.loginService.redirectUrl : '/dashboard';
        // Set our navigation extras object
        // that passes on our global query params and fragment
        let navigationExtras: NavigationExtras = {
          queryParamsHandling: 'preserve',
          preserveFragment: true
        };
        // Redirect the user
        this.router.navigate([redirect], navigationExtras);
      } else {
        this.failLogging = true
        this.message = "Usuario o contraseña incorrectas"
      }
    });
  }

  logout() {
    this.loginService.logout();
    this.setMessage();
  }

}
