import { Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register-step1/register.component';
import { RegistrerStep2Component } from './register/registrer-step2/registrer-step2.component';
import { RegisterStep3Component } from './register/register-step3/register-step3.component';
import { ForgotPassComponent } from './password/forgot-pass/forgot-pass.component';

export const AuthLayoutRoutes: Routes = [
    { path: 'login',          component: LoginComponent },
    { path: 'register',       component: RegisterComponent },
    { path: 'register2',      component: RegistrerStep2Component },
    { path: 'register3',      component: RegisterStep3Component },
    { path: 'recoverpass',      component: ForgotPassComponent },


];
