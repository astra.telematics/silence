import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthLayoutRoutes } from './auth-layout.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register-step1/register.component';
import { RegistrerStep2Component } from './register/registrer-step2/registrer-step2.component';
import { RegisterStep3Component } from './register/register-step3/register-step3.component';
import { RegisterHeaderComponent } from './register/register-header/register-header.component';
import { ForgotPassComponent } from './password/forgot-pass/forgot-pass.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthLayoutRoutes),
    FormsModule,
    // NgbModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    RegistrerStep2Component,
    RegisterStep3Component,
    ForgotPassComponent,
  ],
  exports: [
    LoginComponent,
    RegisterComponent,
    RegistrerStep2Component,
    RegisterStep3Component,
  ]
})
export class AuthLayoutModule { }
