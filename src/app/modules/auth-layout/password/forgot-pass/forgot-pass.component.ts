import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../../../shared/services/login.service';
import { Credential } from 'src/shared/models/Credential';

@Component({
  selector: 'app-forgot-pass',
  templateUrl: './forgot-pass.component.html',
  styleUrls: ['./forgot-pass.component.scss']
})
export class ForgotPassComponent implements OnInit {
  credential: Credential;
  isEmailSend: boolean;
  constructor(public loginService: LoginService) {
    this.credential = new Credential();
  }
  ngOnInit() { }
  sendEmail(){
    this.loginService.recoverPass(this.credential);
    this.isEmailSend = !this.isEmailSend;
  }
}
