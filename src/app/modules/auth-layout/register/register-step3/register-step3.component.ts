import { Component, OnInit } from '@angular/core';
import { Register } from 'src/shared/models/Register';
import { RegisterService } from 'src/shared/services/register-service.service';

@Component({
  selector: 'app-register-step3',
  templateUrl: './register-step3.component.html',
  styleUrls: ['./register-step3.component.scss']
})
export class RegisterStep3Component implements OnInit {
  register: Register;

  constructor(public registerService: RegisterService) {
    this.register = new Register();
  }

  ngOnInit() {
  }

  registerStepThree() {
    this.registerService.registerStepThree(this.register);
  }
}
