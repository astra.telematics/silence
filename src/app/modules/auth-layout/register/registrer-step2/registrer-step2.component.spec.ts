import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrerStep2Component } from './registrer-step2.component';

describe('RegistrerStep2Component', () => {
  let component: RegistrerStep2Component;
  let fixture: ComponentFixture<RegistrerStep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrerStep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrerStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
