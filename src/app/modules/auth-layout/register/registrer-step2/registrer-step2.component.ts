import { Component, OnInit } from '@angular/core';
import { Events } from '../../../../../shared/events/events';
import { Register } from '../../../../../shared/models/Register';
import { RegisterService } from '../../../../../shared/services/register-service.service';

@Component({
  selector: 'app-register2',
  templateUrl: './registrer-step2.component.html',
  styleUrls: ['./registrer-step2.component.scss']
})
export class RegistrerStep2Component implements OnInit {
  register : Register;
  constructor(public registerService : RegisterService ) {
    this.register = new Register();
   }

  ngOnInit() {
  }
  registerStepTwo(){
    this.registerService.registerStepTwo(this.register);   
  }
}


