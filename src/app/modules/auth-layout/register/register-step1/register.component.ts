import { Component, OnInit } from '@angular/core';
import { Register } from 'src/shared/models/Register';
import { RegisterService } from 'src/shared/services/register-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  register : Register;
  constructor(public registerService : RegisterService){ 
    this.register = new Register();
  }

  ngOnInit() {
  }

  registerStepOne(){
    this.registerService.registerStepOne(this.register);
  }

}
