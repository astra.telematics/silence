import { Component, OnInit, Input } from '@angular/core';
import { Events } from 'src/shared/events/events';
import { Scooter } from '../../../../shared/models/Scooter';
import { ActivatedRoute, Router, Route } from '@angular/router';
import { BackendApiService } from 'src/shared/services/backend-api.service';

@Component({
  selector: 'app-scooter-detail',
  templateUrl: './scooter-detail.component.html',
  styleUrls: ['./scooter-detail.component.scss']
})
export class ScooterDetailComponent implements OnInit {
  @Input() scooterData : Scooter; 
  
  constructor(public events : Events, public backendApi : BackendApiService, public route : ActivatedRoute) { 
    this.scooterData = new Scooter();

  }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id')
     
      this.backendApi.getScooterId(id).subscribe((sc : any)  =>{
        this.loadData(sc);
        console.log(sc);
      });

  }
  loadData(e : Scooter){
    this.scooterData = e;
    this.scooterData.name = e.name;
    this.scooterData.labelOne = e.labelOne;
    this.scooterData.labelTwo = e.labelTwo;
    this.scooterData.bateryLvl = e.bateryLvl;
    this.scooterData.bateryState = e.bateryState;
    this.scooterData.autonomy = e.autonomy;
    this.scooterData.maxSpeed = e.maxSpeed;
    this.scooterData.scooterState = e.scooterState;
    this.scooterData.totalkm = e.totalkm;
    this.scooterData.dailyKm = e.dailyKm;
  }
}
