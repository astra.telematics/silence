import { Component, OnInit } from '@angular/core';
import { coordinates } from '../../jsons/coordinates';
import { identifierName } from '@angular/compiler';
import { BackendApiService } from '../../../../shared/services/backend-api.service';
import { Scooter } from '../../../../shared/models/Scooter';
import { Marker } from 'src/shared/models/Marker';
import { Router } from '@angular/router';
declare const google: any;

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})

export class MapsComponent implements OnInit {
  infoScooters: Marker[] = [];
  constructor(public backApi: BackendApiService, public router: Router) {

  }

  ngOnInit() {

    this.backApi.getScooters().subscribe((sc: any) => {
      console.log("MAAAAP DATA" + sc)
      this.createMap(sc);
    })
  }

  createMap(scArray: Array<Scooter>) {
    scArray.forEach((sc: Scooter) => {
      let infoScoot: Marker = {
        lat: sc.coordinate.lat,
        lng: sc.coordinate.lng,
        name: sc.name,
        id: sc.id
      }
      this.infoScooters.push(infoScoot)
    })
  }

  goDetails(id: String) {

    this.router.navigate(['/scooterDetail', id]);
  }
}

