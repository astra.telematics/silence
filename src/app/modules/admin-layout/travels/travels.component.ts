import { Component, OnInit } from '@angular/core';
import { coordinates } from '../../jsons/coordinates';
declare const google: any;


@Component({
  selector: 'app-travels',
  templateUrl: './travels.component.html',
  styleUrls: ['./travels.component.scss']
})
export class TravelsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    let map = document.getElementById('map-canvas');
    let lat = map.getAttribute('data-lat');
    let lng = map.getAttribute('data-lng');
    var myLatlng = new google.maps.LatLng(lat, lng);
    var mapOptions = {
      zoom: 13,
      scrollwheel: false,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    //styles
    }

    map = new google.maps.Map(map, mapOptions);

    var marker = coordinates.scooters.map(info => {
      return new google.maps.Marker({
        position: new google.maps.LatLng(info.coordinate.lat, info.coordinate.lng),
        map: map,
        animation: google.maps.Animation.DROP,
        title: info.name,
        id: info.id,
        icon: {
          path: google.maps.SymbolPath.CIRCLE,
          scale: 8,
          strokeColor: '#2b2b2a',
          strokeWeight: 1,
          fillColor: '#00d86e',
          fillOpacity: 1
        },
      });
    });


    google.maps.event.addListener(marker, 'click', function () {
      console.log("Dentro del evento")
      infowindow.open(map, marker);
    });
    google.maps.event.addListener(map, 'click', function () {
      infowindow.close();
    });
    var contentString = '<div class="info-window-content"><h2>hey</h2>' +
      '<p>A beautiful Dashboard for Bootstrap 4. It is Free and Open Source.</p></div>';

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });


  }

}


//

/* Si quiseramos traer un json completo y coger solo X informacion
coordinates.scooters.forEach(info =>{
      let infoScoot = {
          position :{ lat : info.coordinate.lat, lng : info.coordinate.lng },
          name : info.name,
          id : info.id
      }
      infoScooters.push(infoScoot)
    })

    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(userPos =>{
        let ubication
      })
    }*/
