import { Component, OnInit } from '@angular/core';
import { AddScooters } from '../../../../shared/models/AddScooters';

@Component({
  selector: 'app-add-scooter',
  templateUrl: './add-scooter.component.html',
  styleUrls: ['./add-scooter.component.scss']
})
export class AddScooterComponent implements OnInit {
  stepOne: boolean;
  isEmailSend : boolean;
  addScooters : AddScooters;
  constructor() { 
    this.addScooters = new AddScooters();
  }

  ngOnInit() {
  }

  sendChassisNumber(){
    this.stepOne = !this.stepOne;
    this.isEmailSend = !this.isEmailSend;

  }

  sendCode(){
   // this.stepOne = null;
    //this.isEmailSend = null;


  }

}
