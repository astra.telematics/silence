import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddScooterStep2Component } from './add-scooter-step2.component';

describe('AddScooterStep2Component', () => {
  let component: AddScooterStep2Component;
  let fixture: ComponentFixture<AddScooterStep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddScooterStep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddScooterStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
