import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TablesComponent } from './tables/tables.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ScooterDetailComponent } from './scooter-detail/scooter-detail.component';
import { AddScooterComponent } from './add-scooter/add-scooter.component';
import { TravelsComponent } from './travels/travels.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { AddScooterStep2Component } from './add-scooter/add-scooter-step2/add-scooter-step2.component';
import { AgmCoreModule } from '@agm/core';

// import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule,
    NgxPaginationModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyCoSEwNEhAcZ2YtV9D25MLwqal1FA3Gsm4'
    })  
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TablesComponent,
    IconsComponent,
    MapsComponent,
    ScooterDetailComponent,
    AddScooterComponent,
    TravelsComponent,
    AddScooterStep2Component
  ]
})

export class AdminLayoutModule {}
