import { Component, OnInit, Input } from '@angular/core';
import Chart from 'chart.js';
import { Scooter } from '../../../../shared/models/Scooter';

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from '../../../variables/charts';
import { Router } from '@angular/router';
import { Events } from 'src/shared/events/events';
import { scootersJson } from '../../jsons/scooterJson';
import { BackendApiService } from 'src/shared/services/backend-api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @Input() scooterDetail;
  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: Boolean = true;
  public clicked1: Boolean = false;
  actualPage : number;

  constructor(public router: Router, public events: Events, public backendApi : BackendApiService) {
    this.scooterDetail  = new Array<Scooter>();
  }
  


  ngOnInit() {

    this.backendApi.getScooters().subscribe((sc : any)  =>{
     
      sc.forEach(scot=>{
        const scooterOne = new Scooter();
        scooterOne.name = scot.name;
        scooterOne.id = scot.id;
        scooterOne.labelOne =scot.labelOne;;
        scooterOne.labelTwo = scot.labelTwo;
        scooterOne.bateryState = scot.bateryState;
        scooterOne.scooterState = scot.scooterState;
        scooterOne.bateryLvl =scot.bateryLvl;
        scooterOne.autonomy = scot.autonomy;
        scooterOne.dailyKm = scot.dailyKm;
        this.scooterDetail.push(scooterOne);
    });

    });

    
    this.datasets = [
      [0, 20, 10, 30, 15, 40, 20, 60, 60],
      [0, 20, 5, 25, 10, 30, 15, 40, 40]
    ];
    this.data = this.datasets[0];


    // tslint:disable-next-line: no-var-keyword
    // tslint:disable-next-line: prefer-const
    // tslint:disable-next-line: no-var-keyword
    // tslint:disable-next-line: prefer-const
    // tslint:disable-next-line: prefer-const
    // tslint:disable-next-line: no-var-keyword
    var chartOrders = document.getElementById('chart-orders');

    parseOptions(Chart, chartOptions());


    var ordersChart = new Chart(chartOrders, {
      type: 'bar',
      options: chartExample2.options,
      data: chartExample2.data
    });

    var chartSales = document.getElementById('chart-sales');

    this.salesChart = new Chart(chartSales, {
      type: 'line',
      options: chartExample1.options,
      data: chartExample1.data
    });
  }

  viewDetails(id: string) {

        this.router.navigate(['/scooterDetail',id]);
 
  }
  public updateOptions() {
    this.salesChart.data.datasets[0].data = this.data;
    this.salesChart.update();
  }

}
