import { Component, OnInit, Input } from '@angular/core';
import { Register } from 'src/shared/models/Register';
import { userJson } from '../../jsons/userJson';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  @Input() user: Register ;

  constructor() { 
    this.user = new Register();
  }

  ngOnInit() {
    userJson.user.forEach(u=>{
      this.user.name = u.name;
      this.user.surname = u.surname;
      this.user.phoneNumber = u.phoneNumber;
      this.user.country = u.country;
      this.user.birthdate = u.birthdate;
      this.user.gender = u.gender;
      this.user.credentials.email = u.email;

    })
  }

}
