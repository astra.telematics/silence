import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TablesComponent } from './tables/tables.component';
import { ScooterDetailComponent } from './scooter-detail/scooter-detail.component';
import { AddScooterComponent } from './add-scooter/add-scooter.component';
import { TravelsComponent } from './travels/travels.component';
import { AddScooterStep2Component } from './add-scooter/add-scooter-step2/add-scooter-step2.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'tables',         component: TablesComponent },
    { path: 'scooterDetail/:id',         component: ScooterDetailComponent },
    { path: 'travels',         component: TravelsComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'addScooter',     component: AddScooterComponent },
    { path: 'addScooter2',     component: AddScooterStep2Component },

    { path: 'maps',           component: MapsComponent }
];
