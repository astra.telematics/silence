export var scootersJson = {
    "scooter" : [
        {
        "id": "1",
        "name": "Abuela21",
        "labelOne": "labels",
        "labelTwo": "lobols",
        "bateryState": true,
        "scooterState": true,
        "bateryLvl": 50,
        "autonomy": 30,
        "dailyKm": 20,
        "totalkm" : 10,
        "maxSpeed": 40
    },
    {
        "id": "2",
        "name": "Abuela33",
        "labelOne": "labels",
        "labelTwo": "lobols",
        "bateryState": true,
        "scooterState": true,
        "bateryLvl": 50,
        "autonomy": 30,
        "dailyKm": 90,
        "totalkm" : 30,
        "maxSpeed": 40
    },
    {
        "id": "3",
        "name": "Abuela35",
        "labelOne": "labels",
        "labelTwo": "lobols",
        "bateryState": true,
        "scooterState": true,
        "bateryLvl": 60,
        "autonomy": 70,
        "dailyKm": 50,
        "totalkm" : 20,
        "maxSpeed": 20
    },
    {
        "id": "4",
        "name": "Abuela34",
        "labelOne": "labels",
        "labelTwo": "lobols",
        "bateryState": true,
        "scooterState": true,
        "bateryLvl": 70,
        "autonomy": 20,
        "dailyKm": 10,
        "totalkm" : 30,
        "maxSpeed": 20
    },
]
}