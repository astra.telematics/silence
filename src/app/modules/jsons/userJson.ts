export var userJson = {
    "user" : [
        {
            "id" : "1",
            "name" : "Antonio",
            "surname" : "Gomez",
            "birthdate" : "Enero 21, 1996",
            "phoneNumber" : "666 789 432",
            "country" : "España",
            "gender" : "Masculino",
            "email" : "antonio@gmail.com",
        }
    ]
}