import { Coordinate } from "./Coordinate";

export class Scooter{
    id : string;
    name : string;
    labelOne : string;  
    labelTwo : string;  
    bateryState : boolean;
    scooterState : boolean;
    bateryLvl : number;
    autonomy : number;
    dailyKm : number;
    totalkm : number;
    maxSpeed : number;
    coordinate : Coordinate

}