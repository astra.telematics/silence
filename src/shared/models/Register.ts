import { Credential } from "./Credential";

export class Register{
    company : string;
    name : string;
    surname : string;
    birthdate : string;
    gender : string;
    phoneNumber: string;
    country : string;
    credentials : Credential;
}