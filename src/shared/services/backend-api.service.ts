import { Injectable } from '@angular/core';
import { ConfigService } from '../configuration/config.service';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandlerService } from './http-error-handler.service';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { Scooter } from '../models/Scooter';
import { scootersJson } from 'src/app/modules/jsons/scooterJson';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BackendApiService extends BaseService{
  scooterDetail;
  scooter;
  constructor(configService : ConfigService,http: HttpClient, httpErrorHandler: HttpErrorHandlerService) { 
    super(configService,http, httpErrorHandler);
    this.createHandlerError('RegisterService');
    let scooterDetail = new Array<Scooter>();
  }

  getScooters(){
      let url = this.baseUrl + "scooter"
   return this.http.get(url)
  }


  getScooterId( id: string){
 
    let url = this.baseUrl + "scooter/" + id
    return this.http.get(url)
  }

  
}
