import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Register } from '../models/Register';
import { ConfigService } from '../configuration/config.service';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandlerService } from './http-error-handler.service';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RegisterService extends BaseService{
  public register : Register;
  constructor(configService : ConfigService,http: HttpClient, httpErrorHandler: HttpErrorHandlerService) {
    super(configService,http, httpErrorHandler);
    this.createHandlerError('RegisterService');
    this.register = new Register();
   }

   registerStepOne(formOne : Register){
     this.register.company = formOne.company;
     this.register.name = formOne.name;
     this.register.surname = formOne.surname;
   }
   registerStepTwo(formTwo : Register){
    this.register.birthdate = formTwo.birthdate;
    this.register.gender = formTwo.gender;
    this.register.country = formTwo.country;
    this.register.phoneNumber = formTwo.phoneNumber;
  }

  registerStepThree(formThree : Register){
    this.register.credentials.email = formThree.credentials.email;
    this.register.credentials.password = formThree.credentials.password;

    console.log(this.register);

    this.addRegister();
  }

  addRegister(){
    let url = this.baseUrl + "apiPorCrear";
    return this.http.post(url,this.register).pipe(
      catchError(this.handleError('addRegister',null))
    );
  }
}
