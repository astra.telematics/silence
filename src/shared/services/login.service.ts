import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { ConfigService } from '../configuration/config.service';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandlerService } from './http-error-handler.service';
import { Credential } from '../models/Credential';
import { catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends BaseService{ 
  isLoggedIn = false;
  redirectUrl: string;


  constructor(configService: ConfigService, http: HttpClient, httpErrorHandler: HttpErrorHandlerService) {
    super(configService, http, httpErrorHandler);
    this.createHandlerError('AuthService');
  } 

  login(credential: Credential): Observable<boolean> {
    let url = this.baseUrl + "auth";//TOODO

    return this.http.post<boolean>(url, credential).pipe(
      tap(result => this.isLoggedIn = result),
      catchError(this.handleError('login', null))
    );
  }

  recoverPass(credential : Credential){
    let email = credential.email;
    console.log(email);
    let url = this.baseUrl + "apiPorCrear";//TOODO
    return this.http.post(url,email).pipe(
      catchError(this.handleError('addRegister',null))
    );
  }
  logout(): void {
    this.isLoggedIn = false;
  }

  }

