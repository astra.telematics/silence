import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from '../configuration/config.service';
import { HttpErrorHandlerService, HandleError } from './http-error-handler.service';


export class BaseService {
  
  baseUrl : string;
  handleError: HandleError;
  headers: HttpHeaders;
  options : any;
  
  constructor(public configService : ConfigService,public http: HttpClient, public httpErrorHandler: HttpErrorHandlerService){
      this.baseUrl = configService.GetBaseUrl();  
      this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      this.options = { headers: this.headers };
  }

  createHandlerError(name: string){      
    this.handleError = this.httpErrorHandler.createHandleError(name);
  }
  
}