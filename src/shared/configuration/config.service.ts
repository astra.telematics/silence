import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ConfigService {

    private baseUrl : string;  
  
    constructor() {
        this.baseUrl = "http://localhost:3000/"
    }
  
    GetBaseUrl() : string{
        return this.baseUrl;
    }

}