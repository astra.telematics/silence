import { Injectable, Output, EventEmitter } from '@angular/core';
import { Register } from '../models/Register';
import { Scooter } from '../models/Scooter';



Injectable()
export class Events{
    @Output() showInMap = new EventEmitter();
    @Output() registerSteopOne : EventEmitter<Register> = new EventEmitter();
    @Output() registerSteopTwo : EventEmitter<Register> = new EventEmitter();
    @Output() showDetails : EventEmitter<Scooter> = new EventEmitter();
    @Output() showSC : EventEmitter<Array<Scooter>> = new EventEmitter();


    



}